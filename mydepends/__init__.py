#!/usr/bin/python
# -*- coding:utf-8; mode:python -*-

VERSION = 0.1

import sys
import os
import tempfile
import commands as com
import logging
FORMAT = "[%(levelname)-4s] %(message)s"
logging.basicConfig(level=logging.INFO, format=FORMAT)
log=logging.getLogger('mydepends')

import jinja2
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader('.'))

from optparse import OptionParser
from ConfigParser import ConfigParser

def panic(message):
    log.error(message)
    sys.exit(1)

class Action:
    def __init__(self, conf):
        self.shot_desc = "ACTION WITH NO DESCRIPTION"
        self.cmd       = None
        self.usage     = None
        self.config    = conf
        self.option_parser = OptionParser()

    def parse_args(self, argv):
        if self.usage:
            self.option_parser.usage = "'mydepends %s %s' " % \
                (self.cmd, self.usage)
        else:
            self.option_parser.usage = \
                "'mydepends %s' does not need any option" % self.cmd

        self.options, self.args = self.option_parser.parse_args(argv)

    def run(self, argv):
        self.parse_args(argv)
        return self._run_action()

    def _run_action(self):
        raise NotImplementedError()

class User:
    def __init__(self, fullname, mail):
        self.fullname = fullname
        self.mail = mail

class Metapackage:
    def __init__(self):
        self.name = ''
        self.version = ''
        self.depends = set()

    def __str__(self):
        return '(%s, %s, %s)' % (self.name, self.version, self.depends)

    def gen_control(self, debian_dir, config):

        tmpl_args = dict(mpkg=self,
                         user=User(config.get('Config', 'fullname'),
                                   config.get('Config', 'email')))
        self.__gen_file('control', tmpl_args,
                        os.path.join(debian_dir, 'control'))

    def gen_changelog(self, debian_dir, config, date):

        tmpl_args = dict(mpkg=self,
                         user=User(config.get('Config', 'fullname'),
                                   config.get('Config', 'email')),
                         date=date)

        self.__gen_file('changelog', tmpl_args,
                        os.path.join(debian_dir, 'changelog'))

    def gen_copyright(self, debian_dir, config, date):

        tmpl_args = dict(mpkg=self,
                         user=User(config.get('Config', 'fullname'),
                                   config.get('Config', 'email')),
                         date=date)

        self.__gen_file('copyright', tmpl_args, os.path.join(debian_dir, 'copyright'))

    def gen_rules(self, debian_dir):
        tmpl_args = dict(mpkg=self)
        rules_file = os.path.join(debian_dir, 'rules')
        self.__gen_file('rules', tmpl_args, rules_file)

        s,o = com.getstatusoutput('chmod a+x %s ' % rules_file)

        if s != 0: panic(o)


    def gen_const(self, debian_dir):
        com.getoutput('> %s ' % os.path.join(debian_dir, 'docs'))
        com.getoutput('echo 7 > %s ' % os.path.join(debian_dir, 'compat'))

        source_dir = os.path.join(debian_dir, 'source')
        com.getoutput('mkdir -p %s; echo "3.0 (quilt)" > %s ' %
                      (source_dir, os.path.join(source_dir, 'format')))

        for f in ['docs', 'compat', 'source/format']:
            log.info("['%s'] '%s' file generated at '%s" %
                     (self.name, f, os.path.join(debian_dir, f)))

    def __gen_file(self, template, template_args, dest):
        tmpl = jinja_env.get_template(os.path.join(os.path.dirname(__file__),
                                                   'templates/'+template))

        out = tmpl.render(template_args)
        f = file(dest, 'w'); f.write(out); f.flush(); f.close()
        log.info("['%s'] '%s' file generated at '%s'" % (self.name, template, dest))

class MyDependsConfig(ConfigParser):
    def __init__(self, filename):
        ConfigParser.__init__(self)

        self.conf_file = filename
        self.conf_dir = os.path.abspath(os.path.dirname(filename))

        if not os.path.exists(self.conf_dir):
            os.mkdir(self.conf_dir)
        if not os.path.exists(self.conf_file):
            f = file(self.conf_file, 'w')
            f.close()
        self.read(self.conf_file)

    def update(self):
        log.info("Configuration updated!")
        self.write(file(self.conf_file, 'w'))

    def get_all_mpkgs_names(self):
        retval = []
        for s in self.sections():
            if not s.startswith('mpkg:'):
                continue
            retval.append(s.split(':')[1])
        return retval

    def has_mpkg(self, mpkg_name):
        return self.has_section('mpkg:' + mpkg_name)

    def get_mpkg(self, name):
        if name not in self.get_all_mpkgs_names():
            raise Exception("Meta-Package '%s' not registered" % name)

        retval = Metapackage()
        retval.name = name
        retval.version = self.get('mpkg:'+name, 'version')
        depends = self.get('mpkg:'+name, 'depends')
        retval.depends = set(depends.split())
        return retval

    def update_mpkg(self, mpkg):
        key = 'mpkg:' + mpkg.name
        self.set(key, "depends", '\n'.join(mpkg.depends))
        self.set(key, "version", mpkg.version)

class Initialize(Action):
    def __init__(self, conf):
        Action.__init__(self, conf)
        self.short_desc = "Initialize mydepends configuration."
        self.cmd        = "init"

    def _run_action(self):
        if not self.config.has_section('Config'):
            fullname, email, rootdir = self.get_defaults()
            self.config.add_section("Config")
        else:
            log.warning("Already configured!. Your configuration will be overwritten")
            fullname = self.config.get('Config', 'fullname')
            email    = self.config.get('Config', 'email')
            rootdir  = self.config.get('Config', 'rootdir')

        inputs = [(fullname, "full name"),
                  (email, "email address"),
                  (rootdir, "build root directory")]

        values = []
        for default, desc in inputs:
            val = raw_input("Type your %s ['%s']: " % (desc, default))
            if not val:
                default = values.append(default)
            else:
                values.append(val)

        self.update(*values)

        build_dir = self.config.get('Config', 'rootdir')
        if not os.path.exists(rootdir):
            log.info("Creating directories...")
            os.mkdir(build_dir)

        log.info("done.")
        return 0

    def update(self, fullname, email, rootdir):
        self.config.set("Config", "fullname", fullname)
        self.config.set("Config", "email", email)
        self.config.set("Config", "rootdir", rootdir)
        self.config.update()

    def get_defaults(self):
        cmd = "grep %s /etc/passwd | awk -F ':' '{print $5}'| tr -d ','" % \
            os.environ['USERNAME']
        fullname = com.getoutput(cmd)
        email    = "%s@%s" % (os.environ['USERNAME'],
                              com.getoutput('hostname'))

        return fullname, email, os.path.expanduser('~/mydepends')

class AddMetapkg(Action):
    def __init__(self, conf):
        Action.__init__(self, conf)
        self.short_desc = "Add a new meta-package."
        self.cmd        = "add-mpkg"

        self.option_parser.add_option('-n', '--name',
                                      dest='mpkg_name',
                                      help='Meta-Package name')

        self.option_parser.add_option('-v', '--version',
                                      dest = 'mpkg_version',
                                      default = '1.0',
                                      help = 'Meta-Package version. Default 1.0.')

        self.option_parser.add_option('-f', '--file-dependencies',
                                      dest = 'mpkg_file',
                                      help = 'Meta-Package list dependendies '
                                      '(separated by newlines)'
                                      ' from file.')

        self.usage = \
            '-n MPKG_NAME [-v VERSION] [-f filename | depend1 depend2 ...]'

    def _run_action(self):
        mpkg_name = self.options.mpkg_name

        if not mpkg_name:
            log.error("No package name specified. Use '-n' option")
            return 1

        if self.config.has_mpkg(mpkg_name):
            log.error("Meta-Package '%s' already defined" % mpkg_name)
            return 1


        self.config.add_section('mpkg:' + mpkg_name)
        self.config.set('mpkg:' + mpkg_name, "version", self.options.mpkg_version)

        if not self.options.mpkg_file:
            depends = self.args
        else:
            try:
                f = file(self.options.mpkg_file, 'r')
            except IOError, e:
                log.error("File '%s' could not be opened!" % fname)
                return 1
            depends = [l for l in f.realines()]

        if not depends:
            log.error("No dependencies provided for '%s' meta-package" % mpkg_name)
            return 1

        dep_str=''
        for d in depends:
            dep_str += d+'\n'

        self.config.set('mpkg:' + mpkg_name, "depends", dep_str.strip('\n'))
        self.config.update()

class AddDeps(Action):
    def __init__(self, conf):
        Action.__init__(self, conf)
        self.short_desc = "Add dependencies to a meta-package."
        self.cmd        = "add-deps"

        self.option_parser.add_option('-m', '--meta-package',
                                      dest='mpkg_name',
                                      help='Meta-Package name')

        self.usage = '-m meta-package [depend1 depend2 ...]'

    def _run_action(self):
        if not self.options.mpkg_name:
            log.error('No meta-package name provided')
            return 1

        if not self.args:
            log.error('No dependencies provided')
            return 1

        mpkg_name = self.options.mpkg_name
        mpkg = self.config.get_mpkg(mpkg_name)
        requested_depends = set(self.args)
        repeated_depends = requested_depends.intersection(mpkg.depends)

        if repeated_depends:
            log.warning("Following dependencies are part of '%s': %s"
                        % (mpkg_name, list(repeated_depends)))

        new_depends = requested_depends - repeated_depends

        if not new_depends:
            log.info("No dependencies added to '%s'" % mpkg_name)
            return 0
        try:
            log.info("Adding new dependencies to '%s': %s" % (mpkg_name, ', '.join(new_depends)))
            mpkg.depends = mpkg.depends.union(new_depends)
            self.config.update_mpkg(mpkg)
            self.config.update()
        except Exception,e:
            log.error(e.message)
            return 1

class DelDeps(Action):
    def __init__(self, conf):
        Action.__init__(self, conf)
        self.short_desc = "Delete dependencies from a meta-package."
        self.cmd        = "del-deps"

        self.option_parser.add_option('-m', '--meta-package',
                                      dest='mpkg_name',
                                      help='Meta-Package name')

        self.usage = '-m meta-package [depend1 depend2 ...]'

    def _run_action(self):
        if not self.options.mpkg_name:
            log.error('No meta-package name provided')
            return 1

        if not self.args:
            log.error('No dependencies provided')
            return 1

        mpkg_name = self.options.mpkg_name
        mpkg = self.config.get_mpkg(mpkg_name)
        requested_depends = set(self.args)

        del_depends = requested_depends.intersection(mpkg.depends)

        if not del_depends:
            log.error("None provided dependecies are registered at '%s'" % mpkg.name)
            return 1

        try:
            log.info("Deleting dependencies to '%s': %s" % (mpkg_name, list(del_depends)))
            mpkg.depends = mpkg.depends - del_depends
            self.config.update_mpkg(mpkg)
            self.config.update()
        except Exception,e:
            log.error(e.message)
            return 1

class ShowDeps(Action):
    def __init__(self, conf):
        Action.__init__(self, conf)
        self.short_desc = "Show dependencies from meta-packages."
        self.cmd        = "show-deps"

        self.usage = '[mpkg1 mpkg2...]'

    def _run_action(self):
        if not self.args:
            log.error("No meta-packages provided.")
            return 1

        for p in self.args:
            if not self.config.has_mpkg(p):
                log.warning("Meta-Package '%s' is not registered. Skipped." % p)
                continue

            mpkg = self.config.get_mpkg(p)
            print "%s: %s" % (mpkg.name, ', '.join(mpkg.depends))
        return 0

class DelMetapkg(Action):
    def __init__(self, conf):
        Action.__init__(self, conf)
        self.short_desc = "Delete a meta-package from database."
        self.cmd        = "del-mpkg"

        self.usage = '[options] [mpkg1 [mpkg2 ...]]'

    def _run_action(self):
        if not self.args:
            log.error("No meta-packages to remove from database.")
            return 1

        changed = False
        for p in self.args:
            if not self.config.has_mpkg(p):
                log.warning("Meta-Package '%s' is not registered. Skipped." % p)
                continue

            self.config.remove_section('mpkg:' + p)
            log.info("Meta-Package '%s' removed." % p)
            changed = True

        if changed:
            self.config.update()

        return 0

class Build(Action):
    def __init__(self, conf):
        Action.__init__(self, conf)
        self.short_desc = "Build .deb package."
        self.cmd        = "build"
        self.usage = 'mpkg_name | all'

    def _run_action(self):
        mpkgs = self.config.get_all_mpkgs_names()

        if 'all' in self.args:
            for p in mpkgs: self.__build_mpkg(p)
        else:
            for p in self.args: self.__build_mpkg(p)

        return 0

    def __build_mpkg(self, mpkg_name):
        mpkg = self.config.get_mpkg(mpkg_name)

        build_dir = tempfile.mkdtemp(prefix=mpkg.name+'_')
        debian_dir = os.path.join(build_dir, 'debian')
        try:
            os.mkdir(debian_dir)
        except Exception:
            pass

        date = com.getoutput('date -R')

        mpkg.gen_control(debian_dir, self.config)
        mpkg.gen_changelog(debian_dir, self.config, date)
        mpkg.gen_copyright(debian_dir, self.config, date)
        mpkg.gen_rules(debian_dir)
        mpkg.gen_const(debian_dir)

        log.info("['%s'] creating dummy upstream orig.tar.gz" % mpkg.name)
        s,o = com.getstatusoutput('cd %s; ./debian/rules build-orig-source'
                                  % build_dir)

        if s != 0: panic(o)

        log.info("['%s'] building meta-package" % mpkg.name)
        s,o = com.getstatusoutput('cd %s; dpkg-buildpackage -us -uc -rfakeroot'
                                  % build_dir)

        if s != 0: panic(o)

        rootdir  = self.config.get('Config', 'rootdir')
        s,o = com.getstatusoutput('mv %s/%s_%s* %s' % (os.path.dirname(build_dir),
                                                       mpkg.name, mpkg.version,
                                                       rootdir))
        if s != 0: panic(o)

        log.info("['%s'] moving files to '%s'" % (mpkg.name, rootdir))
        log.info("['%s'] ALL DONE." % mpkg.name)


class List(Action):
    def __init__(self, conf):
        Action.__init__(self, conf)
        self.short_desc = "List availables meta-packages."
        self.cmd        = "list"

    def _run_action(self):
        mpkgs = self.config.get_all_mpkgs_names()

        if not mpkgs:
            log.info("No meta-packages created yet")
            return 0

        print "Meta-Packages availables:"
        for p in mpkgs:
            print "  %s" % p

        return 0


class MyDepends:
    action_classes = [AddMetapkg, DelMetapkg,
                      AddDeps, DelDeps, ShowDeps,
                      Initialize, List, Build]

    def __init__(self, args):
        self.actions = {}
        conf = MyDependsConfig(os.path.expanduser('~/.config/mydepends/mydepends'))
        for c in self.action_classes:
            C = c(conf)
            self.actions[C.cmd] = C

        if len(args) <= 1:
            print "Usage: mydepends action [options]\n"
            self.show_actions_help()
            sys.exit(1)

        self.action  = args[1]
        self.options = args[2:]

        if self.action not in self.actions.keys():
            print "\nAction '%s' not recognized.\n" % self.action
            self.show_actions_help()
            sys.exit(1)

    def show_actions_help(self):
        print "The available actions are:"

        keys = sorted(self.actions.keys())
        for action in keys:
            print "  %-8s --  %s" % (action, self.actions[action].short_desc)

        print "\nUse 'action -h|--help' for more information about the action"

    def run(self):
        return self.actions[self.action].run(self.options)


if __name__ == '__main__':

    app = MyDepends(sys.argv)
    sys.exit(app.run())

